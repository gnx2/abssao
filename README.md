# AbSSAO ver 0.1.1

## About
Angle-Based Ambient OcclusionをMMEに実装したものです。  
対象の点の周囲がどれくらいの角度で開いているかを調べます。  
  
アルゴリズムについて調べるなら"Unreal Engine 4 Ambient Occlusion"で検索すると良いです。  

### Reference
1. http://www.gamasutra.com/view/news/179308/Indepth_Anglebased_SSAO.php
2. http://frederikaalund.com/a-comparative-study-of-screen-space-ambient-occlusion-methods/
3. (2.)からリンクのあるPDF"A Comparative Study of Screen-Space Ambient Occlusion Methods"
4. https://www.unrealengine.com/ja/resourcesの"The Technology Behind the Unreal Engine 4 Elemental Demo"
  
シェーダプログラムは3.に載せられていたFrederik氏によるコードをもとにしました。  
また，MMD向けの実装のために、mqdl氏，おたもん氏，そぼろ氏，針金P氏、によるいくつかの既存SSAOエフェクトを参考にしています。  
  
室内モデルのように閉じた空間は得意ですが広く開けた街モデルは苦手です。  
  
## Contents
 * AbSSAO.x  
   本体ロード用Xファイル
 * AbSSAO.fx  
   本体エフェクト
  
 * full_depth.fxsub  
   深度描画
 * full_normal.fxsub  
   法線描画
 * full_normal_advanced.fxsub  
   法線描画(ノーマルマップテクスチャを持つ材質用)

 * common_settings.fxh  
   各エフェクトファイルをまたぐ共通パラメータ

 * rot_rand.png  
   [0.0,π)でランダムな角度の二次元回転行列を1ピクセルのRGBAに格納

 * README.md
   これ
 * How_to_use.txt
   パラメータの説明


## Platform
それぞれ64bit版の
 * MikuMikuDance 9.22 + MikuMikuEffect 0.37
 * MikuMikuMoving 1.2.6.7
で動作を確認しました。


## Agreement
 * 用途
   * 一切の制限無し
 * スクリプト
   * AbSSAO.fx
     * MIT
   * full_depth.fxsub, full_normal.fxsub, full_normal_advanced.fxsub
     * ベースとした舞力介入Pのfull.fx ver2.0に従う。
 * 免責
   * どのような損害に対しても製作者は責任を負えませんので自己責任でご利用してください。


## Release
 * 2019/04/14  ver 0.1.1
   * gitlabへのソース配置とドキュメント書き換え

 * 2015/08/31  ver 0.1a
   * 32bit MikuMikuEffectでのエラー修正

 * 2015/04/04　ver 0.1
   * 初版


## Contact
 * https://twitter.com/GNX2_
 * https://gitlab.com/gnx2/abssao/issues


## Author
**GNX2**
