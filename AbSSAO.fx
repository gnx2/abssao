/**
 * AbSSAO ver 0.1a
 *
 *　参考
 *  1. http://www.gamasutra.com/view/news/179308/Indepth_Anglebased_SSAO.php
 *  2. http://frederikaalund.com/a-comparative-study-of-screen-space-ambient-occlusion-methods/
 *  3. (2.)からのリンクのあるPDF"A Comparative Study of Screen-Space Ambient Occlusion Methods"
 *  4. https://www.unrealengine.com/ja/resourcesの"The Technology Behind the Unreal Engine 4 Elemental Demo"
 * 
　* シェーダプログラムは3.に載せられていたFrederik氏によるコードをもとにしました。
 * また，MMD向けの実装のために[mqdlさん，おたもんさん，そぼろさん，針金Pさん]によるいくつかの既存SSAOエフェクトを参考にしています。
 *
 * "_Readme.txt" と "使い方.txt" は必ず読んでください。
 *
 * 空間に暗い雰囲気を出すより線を強調します。
 * 室内モデルのように閉じた空間は得意ですが広く開けた街モデルは苦手です。
 * 
 *
 */

// アクセサリ
//  X・・・計測半径に対する加算(デフォルト+X値/100)
//  Y・・・計測距離しきい値に対する加算(デフォルト+Y値)
//  Z・・・ブラー制限距離に対する加算(デフォルト+Z値/10)
//  Si・・・AO生成強度
//  Tr・・・AO合成濃度
// 
//  Rx・・・ブラー範囲に対する加算(デフォルト+Rx値(ラジアン))   -30から30くらい



#include "common_settings.fxh"


// misc
//  MMD最適化率(SiとTrだけでもどうにかなると思う)　0.0から1.0
const float optimize = 0.9;


// AO
//  隠蔽度計算半径(デフォルト)
//  これに(X値/100)を加算する。
const float radius = 0.2;

//  距離しきい値(デフォルト)
//  これにY値を加算する。
const float depth_limit = 5.0;


// ぼかし
// ピクセル幅/ステップ (デフォルト)
//  これにRx値を加算する。
const float extent = 1.0;

//  内積下限制限
const float blur_norm_dot_min = 0.0;

//  距離上限制限(デフォルト)
//  これに(Z値/10)を加算する。
const float blur_depth_max = 0.5;



#ifdef MIKUMIKUMOVING
float radius_MMM
<
    string UIName = "半径";
    string UIHelp = "奥まっているか判断するためにどれくらい離れた所を見るか。";
    string UIWidget = "Slider";
    float UIMin = -20;
    float UIMax = 25.0;
> = float( 0.0 );

float depth_limit_MMM
<
    string UIName = "距離制限";
    string UIHelp = "見た先が前後に離れすぎているところを無効にする。";
    string UIWidget = "Slider";
    float UIMin = -5.0;
    float UIMax = 25.0;
> = float( 0.0 );

float blur_depth_max_MMM
<
    string UIName = "距離制限(ぼかし処理)";
    string UIHelp = "前後に離れすぎている点をぼかしで考慮しない。";
    string UIWidget = "Slider";
    bool UIVisible =  true;
    float UIMin = -5.0;
    float UIMax = 25.0;
> = float( 0.0 );


float extent_MMM
<
    string UIName = "ぼかし範囲";
    string UIHelp = "ピクセル幅";
    string UIWidget = "Slider";
    float UIMin = -1.0;
    float UIMax = 1.0;
> = float( 0.0 );


#endif




//  ステップ数
//  5,7
//  BLUR_SAMP*2+1のブラー
#define BLUR_SAMP 7


// 実装　　(コメントアウトしているものはcommon_settings.fxhにある)
// //  画面端の対策(同フォルダ内のfull_***.fxsubにも同じ値を指定する。)
// #define AO_MAP_SIZE_RATIO 1.125

// //  最大距離(full_depth.fxsubにも同じ値を指定する。)
// #define Z_FAR 8192

//  深度マップ精度
//  16...半, 32...単
#define DEPTH_FP 32

// //  法線マップ要素精度
// //  8...[](32), 10..[](32), 16...半(64), 32...単(128)
// #define NORMAL_FP 10

//  速度のためにアークコサインに近似値を用いる。
//  0...組み込みのacos関数, 1...3次関数近似
#define USE_APPROXIMATE_ACOS 1

//  6組12点(デフォルト)の計測の繰り返し回数
#define SAMP_ITR 1

//  計測サンプル組数
#define SAMP_NUM 6

// 計測方向
const float2 samples[SAMP_NUM] = {
#if SAMP_NUM == 6
    float2(0.03775241352630215f,-0.05660728665612501f),
    float2(0.08919624325171624f,-0.1705317191223313f),
    float2(0.07618731045510917f,-0.34524700393430907f),
    float2(0.5152659841137981f,-0.17549148672096732f),
    float2(-0.26556804689410934f,-0.7128655666902083f),
    float2(0.024888194355149162f,-0.9996902409155249f)

#else
    "INVALID VALUE"

#endif
};



#define M_PI 3.1415926535897932384626433832795



// 確認用
//  接平面クリッピング
//  細かい凹凸まで作りこんでいるか，法線マップがある材質向け
//  しかし正しく実装できていない。
#define NORMAL_TEST 1

//  深度差による点破棄
#define DEPTH_TEST 1

//  デバッグ表示(MMMはタイムラインの場所のエフェクトタブから見ることが出来る。)
//  0...AO合成
//  1...左・・・AOなし，右･･･AO合成
//  2...最終AO
//  3...ブラー前AO
//  4...深度マップ
//  5...法線マップ
#define DEBUG 0




////////////////////////////////////////////////////////////////
// 本体　煮るなり焼くなり
float Script : STANDARDSGLOBAL <
    string ScriptOutput = "color";
    string ScriptClass = "scene";
    string ScriptOrder = "postprocess";
> = 0.8;

// sampler上書き防止
#ifndef MIKUMIKUMOVING
sampler MMDSamp0 : register(s0);
sampler MMDSamp1 : register(s1);
sampler MMDSamp2 : register(s2);
#endif

// 座法変換行列
float4x4 matWV  : WORLDVIEW;
float4x4 matP   : PROJECTION;


float3   CameraDirection   : DIRECTION  < string Object = "Camera"; >;  // calcAngle()で使用。

float AcsTr : CONTROLOBJECT < string name = "(self)"; string item = "Tr"; >;
float AcsSi : CONTROLOBJECT < string name = "(self)"; string item = "Si"; >;
#ifndef MIKUMIKUMOVING
float3 AcsXYZ : CONTROLOBJECT < string name = "(self)"; string item = "XYZ"; >;
float3 AcsRxyz : CONTROLOBJECT < string name = "(self)"; string item = "Rxyz"; >;
#endif

////////////////////////////////////////////////////////////////
// レンダリングターゲットのクリア値
const float4 ClearColor = {1.0,1.0,1.0,1.0};
const float ClearDepth  = 1.0;

// コントローラによる値
static float Strength = AcsSi*0.5f;
static float MinAO = 1.0f-AcsTr;

#ifdef MIKUMIKUMOVING
static float Radius = radius+radius_MMM*0.01f;
static float DepthLimit = depth_limit+depth_limit_MMM;
static float BlurDepthMax = blur_depth_max+blur_depth_max_MMM*0.1f;
static float Extent = extent+extent_MMM;

#else
static float Radius = max(radius+AcsXYZ.x*0.01f,0.0f);
static float DepthLimit = max(depth_limit+AcsXYZ.y,0.0f);
static float BlurDepthMax = max(blur_depth_max+AcsXYZ.z*0.1f,0.0f);
static float Extent = max(extent+AcsRxyz.x,0.0);

#endif

// スクリーンサイズ依存
float2 ViewportSize : VIEWPORTPIXELSIZE;
static float2 ViewportOffset = float2(0.5,0.5)/ViewportSize;

static float2 AOMapSize = floor(ViewportSize*AO_MAP_SIZE_RATIO);
static float2 TrueAOMapRatio = AOMapSize/ViewportSize;
static float2 InvAOMapSize = float2(1.0,1.0)/AOMapSize;
static float2 AOMapOffset = float2(0.5,0.5)/AOMapSize;
static float2 BlurExtent = InvAOMapSize*Extent;

static float InvRatio = 1.0f/AO_MAP_SIZE_RATIO;
static float RatioOffset = (1.0f-InvRatio)*0.5f;

static float4x4 matP_ = float4x4(
    matP[0]/TrueAOMapRatio.x,
    matP[1]/TrueAOMapRatio.y,
    matP[2],
    matP[3]
);

////////////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////////
#define SAMP_MODE(minf, magf, mipf, addu, addv) \
    MinFilter = minf; MagFilter = magf; MipFilter = mipf; AddressU = addu; AddressV = addv;

// 深度バッファ
texture DepthBuffer : RENDERDEPTHSTENCILTARGET <
    float2 ViewPortRatio = {1.0f,1.0f};
    string Format = "D24S8";
>;

// オリジナル
texture OrgMap : RENDERCOLORTARGET <
    float2 ViewPortRatio = {1.0f,1.0f};
    string Format = "A8R8G8B8" ;
    int MipLevels = 1;
>;
sampler OrgSamp = sampler_state {
    texture = <OrgMap>;
    SAMP_MODE(NONE, NONE, NONE, CLAMP, CLAMP)
};

// オフスクリーンレンダー
//  深度
texture DepthMap_AbSSAO : OFFSCREENRENDERTARGET <
    string Description = "DepthMap for AngleBasedAO.fx";
#if DEPTH_FP == 16
    string Format = "R16F";
#elif DEPTH_FP == 32
    string Format = "R32F";
#endif
    float2 ViewPortRatio = {AO_MAP_SIZE_RATIO,AO_MAP_SIZE_RATIO};
    float4 ClearColor = {1.0f,1.0f,1.0f,1.0f};
    float ClearDepth = 1.0f;
    int Miplevels = 0;
    bool AntiAlias = false;
    string DefaultEffect = 
        "self = hide;"
        "* = full_depth.fxsub;"
    ;
>;

sampler DepthSamp = sampler_state {
    texture = <DepthMap_AbSSAO>;
    SAMP_MODE(NONE, NONE, NONE, CLAMP, CLAMP)
};

//  法線
texture NormalMap_AbSSAO : OFFSCREENRENDERTARGET <
    string Description = "NormalMap for AngleBasedAO.fx";

#if NORMAL_FP == 8
    string Format = "X8R8G8B8";
#elif NORMAL_FP == 10
    string Format = "A2R10G10B10";
#elif NORMAL_FP == 16
    string Format = "A16B16G16R16F";
#elif NORMAL_FP == 32
    string Format = "A32B32G32R32F";
#else
    string Format = "INVALID VALUE";
#endif

    float2 ViewPortRatio = {AO_MAP_SIZE_RATIO,AO_MAP_SIZE_RATIO};

#if NORMAL_FP == 8 || NORMAL_FP == 10
    float4 ClearColor = {0.5f,0.5f,0.5f,1.0f};
#elif NORMAL_FP == 16 || NORMAL_FP == 32
    float4 ClearColor = {0.0f,0.0f,0.0f,1.0f};
#else
    float4 ClearColor = {INVALID VALUE};
#endif

    float ClearDepth = 1.0f;
    int Miplevels = 1;
    bool AntiAlias = false;
    string DefaultEffect = 
        "self = hide;"
        "* = full_normal.fxsub;"
    ;
>;

sampler NormalSamp = sampler_state {
    texture = <NormalMap_AbSSAO>;
    SAMP_MODE(NONE, NONE, NONE, CLAMP, CLAMP)
};


// 処理結果
//  AO
texture AOMap : RENDERCOLORTARGET <
    float2 ViewPortRatio = {AO_MAP_SIZE_RATIO,AO_MAP_SIZE_RATIO};
    string Format = "R16F" ;
    int MipLevels = 1;
>;
sampler AOSamp = sampler_state {
    texture = <AOMap>;
    SAMP_MODE(LINEAR, LINEAR, LINEAR, CLAMP, CLAMP)
};

//  ぼかし
texture BluredAOMapX : RENDERCOLORTARGET <
    float2 ViewPortRatio = {AO_MAP_SIZE_RATIO,AO_MAP_SIZE_RATIO};
    string Format = "R16F" ;
    int MipLevels = 1;
>;
sampler BluredAOSampX = sampler_state {
    texture = <BluredAOMapX>;
    SAMP_MODE(NONE, NONE, NONE, CLAMP, CLAMP)
};

texture BluredAOMapXY : RENDERCOLORTARGET <
    float2 ViewPortRatio = {AO_MAP_SIZE_RATIO,AO_MAP_SIZE_RATIO};
    string Format = "R16F" ;
    int MipLevels = 1;
>;
sampler BluredAOSampXY = sampler_state {
    texture = <BluredAOMapXY>;
    SAMP_MODE(LINEAR, LINEAR, NONE, CLAMP, CLAMP)
};


// 乱数テクスチャ RGBAに回転行列
texture2D RandTex < string ResourceName = "rot_rand.png"; >;
sampler2D RandSamp = sampler_state {
    texture = <RandTex>;
    SAMP_MODE(NONE, NONE, NONE, WRAP, WRAP)   
};

static float2 RandTexSize = float2(256.0,256.0);
static float2 InvRandTexSize = float2(1.0,1.0)/RandTexSize;
static float2 RandTexOffset = float2(0.5,0.5)/RandTexSize;
static float2 RandTexRatio = ViewportSize/RandTexSize;


////////////////////////////////////////////////////////////////
// 詳しい事はまたアークエンジェル達にでも訊いてくれ
float acos_(float x) {
#if USE_APPROXIMATE_ACOS == 1
    return (-0.69813170079773212 * x * x - 0.87266462599716477) * x + 1.5707963267948966;
#else
    return acos(x);
#endif
}

// テクスチャ座標 -> 近平面のプロジェクション座標
float2 calcPPosByTexCoord(float2 tc)
{
    return tc*float2(2.0f,-2.0f)+float2(-1.0f,1.0f);
}

// テクスチャ座標とビュー空間Z値 -> ビュー座標 (AOMap用)
float3 calcViewPosOnTexCoord(float2 tc)
{
    float depth = tex2D(DepthSamp,tc).r * Z_FAR;
    float2 temp_ppos = calcPPosByTexCoord(tc);
    float3 view_ray = float3(temp_ppos.x/matP_._11, temp_ppos.y/matP_._22, 1.0);

    return view_ray * depth;
}


// ビュー座標 -> テクスチャ座標 (AOMapサイズ用)
float2 calcTexCoordOnViewPos(float3 vpos)
{
    float4 ppos = mul(float4(vpos,1.0),matP);

    return ppos.xy * float2(0.5,-0.5) + float2(0.5,-0.5);
}

// テクスチャ座標 -> ビュー空間Z値/Z_FAR (AOMapサイズ用)
float getDepthOnTexCoord(float2 tc)
{
    return tex2D(DepthSamp,tc).r;
}

// テクスチャ座標 -> ビュー空間法線方向 (AOMapサイズ用)
float3 getNormalOnTexCoord(float2 tc)
{
#if NORMAL_FP == 8 || NORMAL_FP == 10   // [0.0,1.0] -> [-1.0,1.0]
    return tex2D(NormalSamp,tc).xyz*2.0f - 1.0f;
#else
    return tex2D(NormalSamp,tc).xyz;
#endif
}


////////////////////////////////////////////////////////////////
// 共通 
struct VS_OUTPUT {
    float4 pos : POSITION;
	float2 tc  : TEXCOORD0;
};
////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////
// AO
VS_OUTPUT VS_DrawAOMapSize( float4 pos : POSITION, float2 tc : TEXCOORD0 )
{
    VS_OUTPUT Out = (VS_OUTPUT)0; 
    
    Out.pos = pos;
    Out.tc  = tc + AOMapOffset;
    
    return Out;
}


// 視線ベクトルからの角度
// DrowAOMapからtc_samp_dirを反転させ2回呼び出してその和を周辺の角度の近似とする。
const float bias = 0.05;
static float3 to_cam_vec = -normalize(mul(CameraDirection,(float3x3)matWV));
float calcAngle(float3 vpos,    // カメラ座標系位置
                float3 vnorm,   // カメラ座標系法線
                float2 tc,          // テクスチャ座標
                float2 tc_samp_dir, // 参照方向
                float3 to_cam_vec,  // カメラ方向へのベクトル
                inout float weight)    // 評価重み
{
    float2 tc_a = tc+tc_samp_dir;
    float3 vpos_a = calcViewPosOnTexCoord(tc_a);

    float3 vec_pa = normalize(vpos_a-vpos);


    float cos_angle = dot(to_cam_vec,vec_pa);
    
#if NORMAL_TEST
    // 接平面を考慮
    float norm__pa = dot(vnorm,vec_pa);
    if(norm__pa >= 0.0){    // 狙った効果は出た。しかし不等号が逆ではないのか・・・
        float3 tangent = normalize(vec_pa+norm__pa*vnorm);  // 接平面へ投影したvec_pa
        cos_angle = dot(to_cam_vec,tangent) - bias; // biasは何もないところが暗くなることへの対処
    }

#endif


#if DEPTH_TEST
    // 遠い点を破棄
    float depth_difference = vpos.z-vpos_a.z;
    if(depth_difference > DepthLimit){
        weight -= 0.5;

        // 見た目がわりと良かった，なぜこれでいいのかはわからない
        return lerp(acos_(cos_angle), M_PI/2, saturate((depth_difference-DepthLimit)/DepthLimit));

    }else{
        return acos_(cos_angle);
    }

#endif

    return acos_(cos_angle);
}

float4 PS_DrawAOMap( float2 tc : TEXCOORD0 ) : COLOR0
{
    float3 vpos = calcViewPosOnTexCoord(tc);
    float3 vnorm = getNormalOnTexCoord(tc);
    float depth = getDepthOnTexCoord(tc);


    float angle_sum = 0.0;
    float weight_sum = 0.00001;
    
    float tc_radius = Radius/depth;
    float2 tc_randtex = (tc-AOMapOffset)*RandTexRatio+RandTexOffset;
    

    for(int it=0; it<SAMP_ITR; it++){
        float4 samp_rot = tex2D(RandSamp,tc_randtex)*2.0f-1.0f;

#if SAMP_ITR > 1
        tc_randtex += InvRandTexSize;
#endif
        //[loop]
        for(int i=0; i<SAMP_NUM; i++){
            
            float2 samp_vec = float2(samples[i].x*samp_rot.r+samples[i].y*samp_rot.g,
                                     samples[i].x*samp_rot.b+samples[i].y*samp_rot.a );
            float2 tc_samp_dir = samp_vec*tc_radius*InvAOMapSize;

            float weight = 1.0;
            float angle = calcAngle(vpos,vnorm,tc, tc_samp_dir,to_cam_vec,weight)
                         +calcAngle(vpos,vnorm,tc,-tc_samp_dir,to_cam_vec,weight);

            angle_sum += angle * weight;
            weight_sum += weight;
        }
    }

    float ao = angle_sum/(weight_sum*M_PI);

    return float4(ao,0.0f,0.0f,1.0f);
}
////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////
// ぼかし
// 中心点からの法線内積と距離差に応じた重みでブラー処理

const float WT[BLUR_SAMP+1] = {
#if BLUR_SAMP == 5
    0.1093789154f,
    0.1072130678f,
    0.1009694648f,
    0.0913609498f,
    0.0794253941f,
    0.0663416657f

#elif BLUR_SAMP == 7
    0.0920246254f,
    0.0902024157f,
    0.084949436f,
    0.0768654283f,
    0.0668235931f,
    0.0558157568f,
    0.044793194f,
    0.0345378633f

#else
    "INVALID VALUE"

#endif
};


// smoothstepの乱用はそのうち見直す
float calcBlurWeight(float WT, float norm_dot, float abs_depth_diff)
{
    float norm_wt = smoothstep(blur_norm_dot_min,0.8f,norm_dot);
    float depth_wt = (1.0f-smoothstep(0.0f,BlurDepthMax,abs_depth_diff));
    return WT * norm_wt * depth_wt;
}

// 製作環境で浮動小数点テクスチャのミップマップがサポートされているかわからなかっため
// ミップマップを使ったブラーは未実装
float blur(sampler samp, float2 tc, float2 dir)
{
    float ao_sum = tex2D(samp,tc).r * WT[0];
    float weight_sum = WT[0];

    float3 center_norm = getNormalOnTexCoord(tc);
    float center_detph = getDepthOnTexCoord(tc);
    
    float2 tc1 = tc;
    float2 tc2 = tc;
    [unroll]
    for(int i=1; i<=BLUR_SAMP; i++){
        float3 norm;
        float depth;
        float weight;

        tc1 += dir;
        tc2 -= dir;

        norm = getNormalOnTexCoord(tc1);
        depth = getDepthOnTexCoord(tc1);
        weight = calcBlurWeight(WT[i], dot(center_norm,norm), abs(center_detph-depth)*Z_FAR);

        ao_sum += tex2D(samp,tc1).r*weight;
        weight_sum += weight;


        norm = getNormalOnTexCoord(tc2);
        depth = getDepthOnTexCoord(tc2);
        weight = calcBlurWeight(WT[i], dot(center_norm,norm), abs(center_detph-depth)*Z_FAR);

        ao_sum += tex2D(samp,tc2).r*weight;
        weight_sum += weight;
    }

    return saturate(ao_sum/weight_sum);
}

float4 PS_BlurX( float2 tc : TEXCOORD0) : COLOR0
{
    float ao = blur(AOSamp, tc, float2(BlurExtent.x,0.0f));
    return float4(ao, 0.0f, 0.0f, 1.0f);
}

float4 PS_BlurY( float2 tc : TEXCOORD0) : COLOR0
{
    float ao = blur(BluredAOSampX, tc, float2(0.0f,BlurExtent.y));
    return float4(ao, 0.0f, 0.0f, 1.0f);
}
////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////
// AO合成
//  AO_MAP_SIZE_RATIO倍したところの座標
float2 calcAOMapUvByOrgScreenUv(float2 org_screen_tc)
{
    return (org_screen_tc-ViewportOffset)*InvRatio+RatioOffset+AOMapOffset;
}

//  AO効果を適用
//   SiとTrに基づいて補正してから適用
float4 calcOccludedColor(float ao, float4 color_org)
{
    // 補正
    ao = pow(ao,Strength)*AcsTr+MinAO;  // AcsTr=1-MinAO

    // float4 color;
    // color.rgb = color_org*ao;
    // color.a = color_org.a;

    // float4 color;
    // color = lerp(color_org*MinAO,color_org,ao);
    // color.a = color_org.a;

    float4 color;
    color.rgb = lerp(ao,color_org.rgb,optimize);
    color.rgb = lerp(color.rgb,1.0f,ao)*color_org.rgb;
    color.a = color_org.a;

    return color;
}

// AO結果取得
//  5点サンプル
static const float2 get_ao_samp[4] = {
    float2( InvAOMapSize.x, 0.0f),
    float2(-InvAOMapSize.x, 0.0f),
    float2(0.0f,  InvAOMapSize.y),
    float2(0.0f, -InvAOMapSize.y)
};
float getAO(float2 uv)
{
    float ao = tex2D(BluredAOSampXY,uv).r;
    [unroll]
    for(int i=0; i<4; i++){
        ao += tex2D(BluredAOSampXY, uv+get_ao_samp[i]).r;
    }

    return ao*0.2;;
}


VS_OUTPUT VS_Last( float4 pos : POSITION, float2 tc : TEXCOORD0 )
{
    VS_OUTPUT Out = (VS_OUTPUT)0; 
    
    Out.pos = pos;
    Out.tc  = tc + ViewportOffset;
    
    return Out;
}


float4 PS_Last( float2 tc: TEXCOORD0 ) : COLOR0
{
    float2 uv = calcAOMapUvByOrgScreenUv(tc);

#if DEBUG == 0
    // AO合成
    float ao = getAO(uv);
    float4 color_org = tex2D(OrgSamp,tc);
    float4 color = calcOccludedColor(ao,color_org);   

#elif DEBUG == 1
    //左・・・AOなし，右･･･AO合成
    float4 color_org = tex2D(OrgSamp,tc);
    float4 color;

    if(tc.x>0.501){
        float ao = getAO(uv);
        color = calcOccludedColor(ao,color_org);

    }else if(tc.x<0.499){
        color = color_org;

    }else{
        color = float4(0.0,0.0,0.0,1.0);

    }

#elif DEBUG == 2
    // ブラー後AO
    float ao = tex2D(BluredAOSampXY,uv).r;
    float4 color = float4(ao,ao,ao,1.0f);

#elif DEBUG == 3
    // ブラー前AO
    float ao = tex2D(AOSamp,uv).r;
    float4 color = float4(ao,ao,ao,1.0f);

#elif DEBUG == 4
    // 深度
    float  d = tex2D(DepthSamp,uv).r;
    float4 color = float4(d,d,d,1.0f);

#elif DEBUG == 5
    // 法線
    float4 color = tex2D(NormalSamp,uv);

#endif

    return color;
}
////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////
technique AbSSAO <
    string Script = 

        "RenderColorTarget0=OrgMap;"
        "RenderDepthStencilTarget=DepthBuffer;"
            "ClearSetColor=ClearColor;"
            "ClearSetDepth=ClearDepth;"
                "Clear=Color;"
                "Clear=Depth;"
                "ScriptExternal=Color;"

        "RenderColorTarget0=AOMap;"
        "RenderDepthStencilTarget=DepthBuffer;"
            "ClearSetColor=ClearColor;"
            "ClearSetDepth=ClearDepth;"
                "Clear=Color;"
                "Clear=Depth;"
                "Pass=DrawAOMap;"

        "RenderColorTarget0=BluredAOMapX;"
        "RenderDepthStencilTarget=DepthBuffer;"
            "ClearSetColor=ClearColor;"
        "ClearSetDepth=ClearDepth;"
                "Clear=Color;"
                "Clear=Depth;"
                "Pass=BlurAOMapX;"

        "RenderColorTarget0=BluredAOMapXY;"
        "RenderDepthStencilTarget=DepthBuffer;"
            "ClearSetColor=ClearColor;"
            "ClearSetDepth=ClearDepth;"
                "Clear=Color;"
                "Clear=Depth;"
                "Pass=BlurAOMapXY;"

        "RenderColorTarget0=;"
        "RenderDepthStencilTarget=;"
            "ClearSetColor=ClearColor;"
            "ClearSetDepth=ClearDepth;"
                "Clear=Color;"
                "Clear=Depth;"
                "Pass=DrawLast;"

    ;
> {

    pass DrawAOMap < string Script= "Draw=Buffer;"; > {
        AlphaBlendEnable = false;
        VertexShader = compile vs_3_0 VS_DrawAOMapSize();
        PixelShader  = compile ps_3_0 PS_DrawAOMap();
    }

    pass BlurAOMapX < string Script= "Draw=Buffer;"; > {
        AlphaBlendEnable = false;
        VertexShader = compile vs_3_0 VS_DrawAOMapSize();
        PixelShader  = compile ps_3_0 PS_BlurX();
    }

    pass BlurAOMapXY < string Script= "Draw=Buffer;"; > {
        AlphaBlendEnable = false;
        VertexShader = compile vs_3_0 VS_DrawAOMapSize();
        PixelShader  = compile ps_3_0 PS_BlurY();
    }


    pass DrawLast < string Script= "Draw=Buffer;"; > {
        AlphaBlendEnable = false;
        VertexShader = compile vs_3_0 VS_Last();
        PixelShader  = compile ps_3_0 PS_Last();
    }

}
////////////////////////////////////////////////////////////////

// 製作:GNX2
